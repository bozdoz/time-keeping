<?php
include 'functions.php';
if (isset($_GET['log'])) {
	$mysqli = connect();
	$type = strtoupper($mysqli->real_escape_string($_GET['log']));
	$minutes = 'NULL';

	/* make sure it's not readding the same command:
	types should alternate */
	$sql = "
		SELECT type 
		FROM time
		ORDER BY time DESC
		LIMIT 1
	";

	if ($res = mysqli_query($mysqli, $sql)) {
		while ($row = mysqli_fetch_assoc($res)) {
			if ($row['type'] == $type) {
				exit();
			}
		}
	}

	if ($type == 'OUT') {
		/*execute more sql to get difference in time*/
		$sql = "
			SELECT time 
			FROM time
			WHERE type = 'IN'
			ORDER BY time DESC
			LIMIT 1
		";

		if ($res = mysqli_query($mysqli, $sql)) {
			while ($row = mysqli_fetch_assoc($res)) {
				$last_time = new DateTime($row['time']);
			}
		}
		$seconds = date_diff(new DateTime(), $last_time);
		$minutes = intval($seconds/60);
	} elseif ($type == 'IN') {
		/*only other viable option*/
	} else {
		echo 'none';
		exit();
	}
	$sql = "
		INSERT INTO `time` (type, minutes)
		VALUES ('$type', $minutes)
	";

	if($res = mysqli_query($mysqli, $sql)){
		echo "logged $type";
	} else {
		echo "Error logging $type";
		email_admins('Time Tracking Error', "Error logging $type with $minutes minutes");
	}
} else {
	echo 'none';
}
?>