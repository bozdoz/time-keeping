<?php
include 'header.php';
include 'functions.php';
$mysqli = connect();

$sql = "
	SELECT *, ( 
		SELECT COUNT(id) 
		FROM `time` 
		) as count 
	FROM `time`
	ORDER BY time DESC
";

$num_per_page = 40;
if (isset($_GET['num'])) {
	$num_per_page = intval($mysqli->real_escape_string($_GET['num']));
}
$getnum = 0;
if (isset($_GET['page'])) {
	$getnum = intval($mysqli->real_escape_string($_GET['page']));
}
$sql .= " LIMIT $getnum, $num_per_page";

$punches = Array();

if($res = mysqli_query($mysqli, $sql)){
	while($row = mysqli_fetch_assoc($res)){
		$punches[] = $row;
	}
}
mysqli_free_result($res);
?>
<table class='table table-hover'>
<tr><th>Day - Time</th><th>Type</th><th>Gurth</th></tr>
<?php
foreach ($punches as $punch) {
?>
<tr>
<?php 
$timestamp = strtotime('-2 hours', strtotime($punch['time']));
$this_day = date('D, M j, Y', $timestamp);
if (!(isset($day) && $day == $this_day)) {
	/* add up previous day */
	if (isset($day)) {
		echo '<th colspan="2" class="text-right">Day Total :</th><th colspan="1" class="time">';
		echo output_hours_minutes($days_minutes);
		echo '</th></tr><tr>'; //tricky tricky	
	}
	$day = $this_day;
	$days_minutes = 0; // reset
	echo '<td colspan="3" class="success">';
	echo $this_day;
	echo '</td></tr><tr>'; //tricky tricky
}
?>
<td><?php 
echo date('g:i a', $timestamp);
?></td>
<?php 
$punchtype = $punch['type'];
echo "<td>$punchtype</td>";
if ($punchtype == 'OUT') {
	$minutes = $punch['minutes'];
	$days_minutes += $minutes;
 	echo '<td rowspan="2" class="time">';
	output_hours_minutes($minutes);
	echo '</td>';
}
echo '</tr>';

} // end foreach

if (isset($day)) {
	echo '<tr><th colspan="2" class="text-right">Total (on this page) :</th><th colspan="1" class="time">';
	echo output_hours_minutes($days_minutes);
	echo '</th></tr>'; //tricky tricky	
}
?>
</table>
<?php
$paginate = '';
$count = $punches[0]['count'];
if (isset($_GET['page'])) {
	$prev_num = ($_GET['page']==$num_per_page) ? '0' : ($_GET['page']-$num_per_page);
	
	$paginate .= '<a href="/?num='.$num_per_page.'" class="btn btn-warning">&larr; First '. $num_per_page.'</a>';
	if ($prev_num != '0') {
		$paginate .= '<a href="/?page='.$prev_num.'&num='.$num_per_page.'" class="btn btn-warning">&larr; Previous '. $num_per_page . '</a>';
	}
	
	if ($_GET['page'] < $count - $num_per_page) {
		$next_num = $_GET['page'] + $num_per_page;
		$paginate .= '<a href="/?page='.$next_num.'&num='.$num_per_page.'" class="btn btn-warning">Next '.$num_per_page.' &rarr;</a>';
		if ($next_num != $count - $num_per_page) {
			$paginate .= '<a href="/?page='.($count - $num_per_page).'&num='.$num_per_page.'" class="btn btn-warning">Last '.$num_per_page.' &rarr;</a>';
		}
	}
} elseif ($count < $num_per_page) {
	$paginate = '';
} else {
	$paginate .= '<a href="/?page='.$num_per_page.'&num='.$num_per_page.'" class="btn btn-warning">Next '.$num_per_page.' &rarr;</a>';
	$paginate .= '<a href="/?page='.($count - $num_per_page).'&num='.$num_per_page.'" class="btn btn-warning">Last '.$num_per_page.' &rarr;</a>';
}

echo $paginate;
include 'footer.php';
?>