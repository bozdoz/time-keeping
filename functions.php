<?php
include 'mysql.php';

function connect () {
	global $mysql_host, $mysql_database, $mysql_password, $mysql_user;
	return new mysqli($mysql_host, $mysql_user, $mysql_password, $mysql_database);
}

function date_diff ($date1, $date2) {
	return $date1->format('U') - $date2->format('U');
}

function email_admins ($subject, $message) {
	global $admin_email;
	if (!empty($admin_email)) {
		$to      = $admin_email;
		$subject = empty($subject) ? 'Emailing admins' : $subject;
		$message = empty($message) ? 'Emailing admins' : $message;
		$headers = "From: $admin_email\r\n" .
		    "Reply-To: $admin_email\r\n" .
		    "X-Mailer: PHP/" . phpversion();

		mail($to, $subject, $message, $headers);
	}
}

function output_hours_minutes ($minutes) {
	$hours = intval($minutes / 60);
	if ($hours == 0) {
		$hours = '';
	} else {
		$hours .= ($hours == 1) ? ' hour' : ' hours';
	}
	$minutes = intval($minutes % 60);
	if ($minutes == 0) {
		$minutes = '';
	} else {
		$minutes .= ($minutes == 1) ? ' minute' : ' minutes';
		$hours .= empty($hours) ? '' : ', ';
	}
	if ($hours == '' && $minutes == '') {
		$minutes = '0 minutes';
	}

	echo "$hours$minutes";
}

/*possible fix sql for adding a new entry and then fixing the next one */
/*
INSERT INTO time (time, type)
VALUES ('2014-02-13 02:00:00', 'IN')

SELECT * FROM time
WHERE time < '2014-02-13 02:14:57'
ORDER BY time DESC
LIMIT 1*/